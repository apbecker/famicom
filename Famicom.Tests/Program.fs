module Famicom.Tests.Program

open Expecto

[<EntryPoint>]
let main argv =
  runTestsInAssembly defaultConfig argv
