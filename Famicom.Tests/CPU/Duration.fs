module Famicom.Tests.Duration

open Expecto
open Hedgehog
open Famicom.CPU.Audio


let testProperty name prop =
  testCase name <| fun () -> Property.check prop


let duration =
  Gen.constant (Duration.init ())


[<Tests>]
let tests =
  testList "duration"
    [ testProperty "counter stops decrementing at zero" <| property {
        let duration = Duration.init ()

        Duration.tick duration

        return duration.counter = 0
      }


      testProperty "counter stops decrementing while halt is true" <| property {
        let! counter = Gen.int (Range.linear 0 255)
        let duration = Duration.init ()

        duration.counter <- counter
        duration.halted <- true
        Duration.tick duration

        return duration.counter = counter
      }


      testProperty "counter decrements if not halted or zero" <| property {
        let! counter = Gen.int (Range.linear 1 255)
        let duration = Duration.init ()

        duration.counter <- counter
        duration.halted <- false
        Duration.tick duration

        return duration.counter < counter
      }
    ]
