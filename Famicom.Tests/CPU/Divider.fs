module Famicom.Tests.Divider

open Expecto
open Hedgehog
open Famicom.CPU.Audio


let testProperty name prop =
  testCase name <| fun () -> Property.check prop


let period =
  Gen.int (Range.linear 0 0x7ff)


[<Tests>]
let tests =
  testList "cpu divider"
    [ testProperty "adding 0 cycles has no effect" <| property {
        let! P = period
        let divider = Divider.init P

        divider
          |> Divider.tickMany 0
          |> ignore

        return divider = (Divider.init P)
      }


      testProperty "period is not P" <| property {
        let! P = period
        let outputs = Divider.init P |> Divider.tickMany P
        return outputs = 0
      }


      testProperty "period is P+1" <| property {
        let! P = period
        let outputs = Divider.init P |> Divider.tickMany (P + 1)
        return outputs = 1
      }


      testProperty "period 0 disables dividing" <| property {
        let! T = Gen.int (Range.linear 0 100)
        let outputs = Divider.init 0 |> Divider.tickMany T
        return outputs = T
      }


      testProperty "adding T cycles agrees with 'tick'" <| property {
        let! P = period
        let! T = Gen.int (Range.linear 0 100)

        let dividerA = Divider.init P
        let outputsA = Divider.tickMany T dividerA

        let dividerB = Divider.init P
        let outputsB =
          Seq.init T (fun _ -> 1)
            |> Seq.filter (fun _ -> Divider.tick dividerB)
            |> Seq.sum

        return (dividerA, outputsA) = (dividerB, outputsB)
      }


      testProperty "adding a list of cycles individually is the same as adding the sum of list" <| property {
        let! inputs = Gen.list (Range.linear 1 10) (Gen.int (Range.linear 1 40))
        let! P = period

        let dividerA = Divider.init P
        let outputsA =
          inputs
            |> Seq.sumBy (fun i -> Divider.tickMany i dividerA)

        let dividerB = Divider.init P
        let outputsB = Divider.tickMany (Seq.sum inputs) dividerB

        return (dividerA, outputsA) = (dividerB, outputsB)
      }
    ]
