namespace Famicom.Input

type Joypad =
  | NotConnected
  | StandardController of StandardController

module Joypad =

  let create index =
    StandardController.create(index)
      |> StandardController


  let getData strobe =
    function
    | NotConnected ->
      0uy

    | StandardController(ctrl) ->
      StandardController.getData strobe ctrl


  let setData =
    function
    | NotConnected ->
      ()

    | StandardController(ctrl) ->
      StandardController.setData ctrl


  let frame =
    function
    | NotConnected ->
      ()

    | StandardController(ctrl) ->
      StandardController.frame ctrl
