namespace Famicom.Input

open Famicom.Platform.Input

type StandardController =
  { input: InputBackend
    state: bool []
    mutable index: int
    mutable latch: byte
    mutable value: byte }

module StandardController =

  let create index =
    let input = InputBackend(index, 10)

    input.Map 0 "A"
    input.Map 1 "X"
    input.Map 2 "Back"
    input.Map 3 "Menu"
    input.Map 4 "DPad-U"
    input.Map 5 "DPad-D"
    input.Map 6 "DPad-L"
    input.Map 7 "DPad-R"
    input.Map 8 "B"
    input.Map 9 "Y"

    { input = input
      state = Array.zeroCreate 8
      index = 0
      latch = 0uy
      value = 0uy }


  let getData strobe ctrl =
    let temp = ctrl.value &&& 1uy

    if strobe = 0 then
      ctrl.value <- (ctrl.value >>> 1) ||| 0x80uy

    temp


  let setData ctrl =
    ctrl.value <- ctrl.latch


  let frame ctrl =
    ctrl.input.Update()
    ctrl.index <- 0
    ctrl.latch <- 0uy

    for i in 0 .. 7 do
      ctrl.state.[i] <- ctrl.input.Pressed(i)
