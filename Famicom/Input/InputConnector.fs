namespace Famicom.Input

type InputConnector =
  { joypad1: Joypad
    joypad2: Joypad
    mutable strobe: int }

module InputConnector =

  let init joypad1 joypad2 =
    { joypad1 = joypad1
      joypad2 = joypad2
      strobe = 0 }


  let readJoypad1 inputConnector =
    inputConnector.joypad1
      |> Joypad.getData inputConnector.strobe
    

  let readJoypad2 inputConnector =
    inputConnector.joypad2
      |> Joypad.getData inputConnector.strobe
    

  let write inputConnector (data: byte) =
    inputConnector.strobe <- (int data) &&& 1

    if inputConnector.strobe = 0 then
      inputConnector.joypad1 |> Joypad.setData
      inputConnector.joypad2 |> Joypad.setData
    

  let frame inputConnector =
    inputConnector.joypad1 |> Joypad.frame
    inputConnector.joypad2 |> Joypad.frame
