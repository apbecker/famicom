namespace Famicom.PPU

type Scroll =
  { mutable swap: bool
    mutable fine: int
    mutable step: int
    mutable address: int
    mutable latch: int }

module Scroll =

  let init () =
    { swap = false
      fine = 0
      step = 1
      address = 0
      latch = 0 }


  let private putAddress scroll address =
    scroll.address <- address
    scroll


  let clockX scroll =
    let address =
      if (scroll.address &&& 0x001f) = 0x001f then
        scroll.address ^^^ 0x041f
      else
        scroll.address + 0x0001

    address |> putAddress scroll


  let clockY scroll =
    let address =
      if (scroll.address &&& 0x7000) = 0x7000 then
        match scroll.address &&& 0x3e0 with
        | 0x3a0 -> scroll.address ^^^ 0x7ba0
        | 0x3e0 -> scroll.address ^^^ 0x73e0
        | _ ->
          (scroll.address ^^^ 0x7000) + 0x20
      else
        scroll.address + 0x1000

    address |> putAddress scroll


  let resetX scroll =
    (scroll.address &&& 0x7be0) ||| (scroll.latch &&& 0x041f)
      |> putAddress scroll


  let resetY scroll =
    (scroll.address &&& 0x041f) ||| (scroll.latch &&& 0x7be0)
      |> putAddress scroll


  let tick (h, v) scroll =
    if (h &&& 0x107) = 0x007 || (h &&& 0x1f7) = 0x147 then
      scroll |> clockX |> ignore

    if h = 255 then scroll |> clockY |> ignore
    if h = 256 then scroll |> resetX |> ignore

    if v = 261 && (h >= 280 && h <= 304) then
      scroll |> resetY |> ignore
