module Famicom.PPU.BgUnit

let private pixel = Array.zeroCreate<int> (256 + 16)


let fetchName data state =
  state.fetchName <- data


let fetchAttr data state =
  let x = (state.scroll.address >>> 0) &&& 2
  let y = (state.scroll.address >>> 5) &&& 2
  let shift = (y <<< 1) ||| x

  state.fetchAttr <- data >>> shift


let fetchBit0 data state =
  state.fetchBit0 <- data


let fetchBit1 data state =
  state.fetchBit1 <- data


let pointName state =
  state.fetchAddress <- 0x2000 ||| (state.scroll.address &&& 0xfff)


let pointAttr state =
  let x = ((state.scroll.address >>> 2) &&& 7)
  let y = ((state.scroll.address >>> 4) &&& 0x38)

  state.fetchAddress <- 0x23c0 ||| (state.scroll.address &&& 0xc00) ||| y ||| x


let pointBit0 state =
  let line = (state.scroll.address >>> 12) &&& 7
  state.fetchAddress <- state.bkgAddress ||| ((int state.fetchName) <<< 4) ||| 0 ||| line


let pointBit1 state =
  let line = (state.scroll.address >>> 12) &&& 7
  state.fetchAddress <- state.bkgAddress ||| ((int state.fetchName) <<< 4) ||| 8 ||| line


let synthesize state =
  let offset = (state.h + 9) % 336

  for i in 0 .. 7 do
    pixel.[offset + i] <-
      (((int state.fetchAttr) <<< 2) &&& 12) |||
      (((int state.fetchBit0) >>> 7) &&&  1) |||
      (((int state.fetchBit1) >>> 6) &&&  2)

    state.fetchBit0 <- state.fetchBit0 <<< 1
    state.fetchBit1 <- state.fetchBit1 <<< 1


let getPixel state =
  if not state.bkgEnabled || (state.bkgClipped && state.h < 8) then
    0
  else
    pixel.[state.h + state.scroll.fine]
