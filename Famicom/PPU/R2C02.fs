module Famicom.PPU.R2C02

open Famicom
open Famicom.Memory
open Famicom.Platform.Hosting


let init state =
  SpUnit.evaluationReset state
  SpUnit.initializeSprite ()


let rendering state =
  (state.bkgEnabled || state.objEnabled) && state.v < 240


let vbl sendVBL state =
  sendVBL (state.vblFlag && state.vblEnabled)


let evaluationTick state =
  match state.h &&& 1 with
  | 0 -> SpUnit.evaluation0 state
  | 1 -> SpUnit.evaluation1 state
  | _ ->
    ()

  if state.h = 0x3f then SpUnit.evaluationBegin state
  if state.h = 0xff then SpUnit.evaluationReset state


let bgTick read state step =
  match step with
  | 0 ->
    state |> BgUnit.pointName

  | 1 ->
    state |> BgUnit.fetchName (read state.fetchAddress)

  | 2 ->
    state |> BgUnit.pointAttr

  | 3 ->
    state |> BgUnit.fetchAttr (read state.fetchAddress)

  | 4 ->
    state |> BgUnit.pointBit0

  | 5 ->
    state |> BgUnit.fetchBit0 (read state.fetchAddress)

  | 6 ->
    state |> BgUnit.pointBit1

  | 7 ->
    state |> BgUnit.fetchBit1 (read state.fetchAddress)
    state |> BgUnit.synthesize

  | _ ->
    ()


let spTick read state step =
  match step with
  | 0 ->
    state |> BgUnit.pointName

  | 1 ->
    state |> BgUnit.fetchName (read state.fetchAddress)

  | 2 ->
    state |> BgUnit.pointAttr

  | 3 ->
    state |> BgUnit.fetchAttr (read state.fetchAddress)

  | 4 ->
    state |> SpUnit.pointBit0

  | 5 ->
    state |> SpUnit.fetchBit0 (read state.fetchAddress)

  | 6 ->
    state |> SpUnit.pointBit1

  | 7 ->
    state |> SpUnit.fetchBit1 (read state.fetchAddress)
    state |> SpUnit.synthesize

  | _ ->
    ()


let colorMultiplexer state bkg obj =
  match struct (bkg &&& 3, obj &&& 3) with
  | 0, obj -> obj
  | bkg, 0 -> bkg
  | _ ->
    if (obj &&& 0x4000) <> 0 then
      state.objZeroHit <- true

    if (obj &&& 0x8000) <> 0 then
      bkg
    else
      obj


let renderPixel video state =
  let bkg = BgUnit.getPixel state
  let obj = SpUnit.getPixel state
  let pixel = colorMultiplexer state bkg obj
  let index = CGRAM.read state.cgram pixel
  let color = Palette.color (((int index) &&& state.clipping) ||| state.emphasis)

  VideoSink.run video state.h state.v (int color)


let activeCycle read video state =
  let h = state.h

  if (h &&& 0x100) = 0x000 then
    bgTick read state (h &&& 7)
    renderPixel video state
    evaluationTick state
  elif (h &&& 0x1c0) = 0x100 then spTick read state (h &&& 7)
  elif (h &&& 0x1f0) = 0x140 then bgTick read state (h &&& 7)
  elif (h &&& 0x1fc) = 0x150 then bgTick read state (h &&& 1)

  state.scroll |> Scroll.tick (state.h, state.v)


let tick sendVBL state =
  if state.v = 240 && state.h = 340 then state.vblHold <- true
  if state.v = 241 && state.h =   0 then state.vblFlag <- state.vblHold
  if state.v = 241 && state.h =   2 then vbl sendVBL state

  if state.v = 260 && state.h = 340 then
    state.objOverflow <- false
    state.objZeroHit <- false

  if state.v = 260 && state.h = 340 then state.vblHold <- false
  if state.v = 261 && state.h =   0 then state.vblFlag <- state.vblHold
  if state.v = 261 && state.h =   2 then vbl sendVBL state

  state.h <- state.h + 1


let bufferCycle sendVBL read state =
  let h = state.h

  if (h &&& 0x100) = 0x000 then bgTick read state (h &&& 7)
  elif (h &&& 0x1c0) = 0x100 then spTick read state (h &&& 7)
  elif (h &&& 0x1f0) = 0x140 then bgTick read state (h &&& 7)
  elif (h &&& 0x1fc) = 0x150 then bgTick read state (h &&& 1)

  state.scroll |> Scroll.tick (state.h, state.v)

  if h = 337 && state.field then
    tick sendVBL state


let forcedBlankPixel state =
  if (state.scroll.address &&& 0x3f00) = 0x3f00 then
    state.scroll.address
  else
    0


let forcedBlankCycle video state =
  if (state.v < 240 && state.h < 256) then
    let pixel = forcedBlankPixel state
    let index = CGRAM.read state.cgram pixel
    let color = Palette.color (((int index) &&& state.clipping) ||| state.emphasis)

    VideoSink.run video state.h state.v (int color)


let update video sendVBL read state =
  if (state.bkgEnabled || state.objEnabled) then
    if (state.v < 240) then activeCycle read video state
    if (state.v = 261) then bufferCycle sendVBL read state
  else
    forcedBlankCycle video state

  tick sendVBL state

  if state.h = 341 then
    state.h <- 0
    state.v <- state.v + 1

    if (state.v = 261) then
      state.field <- not state.field

    if (state.v = 262) then
      state.v <- 0


let clock video sendVBL read state amount =
  let single = 44

  state.cycles <- state.cycles + amount

  while state.cycles >= single do
    state.cycles <- state.cycles - single
    update video sendVBL read state
