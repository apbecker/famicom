namespace Famicom.PPU

type Sprite =
  { mutable Y: int
    mutable Name: int
    mutable Attr: int
    mutable X: int }

module Sprite =

  let [<Literal>] FlipV = 0x80
  let [<Literal>] FlipH = 0x40
  let [<Literal>] Priority = 0x20
  let [<Literal>] SpriteZero = 0x10

  let init () =
    { Y = 0xff
      Name = 0xff
      Attr = 0xe3
      X = 0xff }


module SpUnit =

  open Famicom.Platform.Util


  let pixel = Array.zeroCreate<int> 256
  let spFound = ArrayHelper.create1 (fun _ -> Sprite.init ()) 8


  let mutable spLatch = 0
  let mutable spCount = 0
  let mutable spIndex = 0
  let mutable spPhase = 0


  let synthesize state =
    if state.v = 261 then
      ()
    else
      let sprite = spFound.[(state.h >>> 3) &&& 7]

      let rec loop (i, offset) =
        if (i < 8 && offset < 256) then
          let color =
              ((state.fetchBit0 >>> 7) &&& 1uy) |||
              ((state.fetchBit1 >>> 6) &&& 2uy)

          if (pixel.[offset] &&& 3) = 0 && color <> 0uy then
            pixel.[offset] <-
              0x10 |||
              ((sprite.Attr <<< 10) &&& 0xc000) |||
              ((sprite.Attr <<<  2) &&& 0x000c) |||
              (int color)

          state.fetchBit0 <- state.fetchBit0 <<< 1
          state.fetchBit1 <- state.fetchBit1 <<< 1
          loop (i + 1, offset + 1)

      loop (0, sprite.X)


  let evaluation0 state =
    if state.h < 64 then
      spLatch <- 0xff
    else
      spLatch <- int <| state.oam.[int state.oamAddress]


  let evaluation1 state =
    if state.h < 64 then
      match (state.h >>> 1) &&& 3 with
      | 0 -> spFound.[(state.h >>> 3) &&& 7].Y <- spLatch
      | 1 -> spFound.[(state.h >>> 3) &&& 7].Name <- spLatch
      | 2 -> spFound.[(state.h >>> 3) &&& 7].Attr <- spLatch &&& 0xe3
      | 3 -> spFound.[(state.h >>> 3) &&& 7].X <- spLatch
      | _ ->
        ()
    else
      match spPhase with
      | 0 ->
        spCount <- spCount + 1

        let raster = (state.v - (int spLatch)) &&& 0x1ff
        if raster < state.objRasters then
          state.oamAddress <- state.oamAddress + 1uy
          spFound.[spIndex].Y <- spLatch
          spPhase <- spPhase + 1
        else
          if spCount <> 64 then
            state.oamAddress <- state.oamAddress + 4uy
          else
            state.oamAddress <- 0uy
            spPhase <- 8

      | 1 ->
        state.oamAddress <- state.oamAddress + 1uy
        spFound.[spIndex].Name <- spLatch
        spPhase <- spPhase + 1

      | 2 ->
        state.oamAddress <- state.oamAddress + 1uy
        spFound.[spIndex].Attr <- spLatch &&& 0xe3
        spPhase <- spPhase + 1

        if spCount = 1 then
          spFound.[spIndex].Attr <- spFound.[spIndex].Attr ||| Sprite.SpriteZero

      | 3 ->
        spFound.[spIndex].X <- spLatch
        spIndex <- spIndex + 1

        if spCount <> 64 then
          spPhase <- if (spIndex <> 8) then 0 else 4
          state.oamAddress <- state.oamAddress + 1uy
        else
          spPhase <- 8
          state.oamAddress <- 0uy

      | 4 ->
        let raster = (state.v - (int spLatch)) &&& 0x1ff
        if raster < state.objRasters then
          state.objOverflow <- true
          spPhase <- spPhase + 1
          state.oamAddress <- state.oamAddress + 1uy
        else
          state.oamAddress <- byte (((state.oamAddress + 4uy) &&& ~~~3uy) + ((state.oamAddress + 1uy) &&& 3uy))

          if state.oamAddress <= 5uy then
            spPhase <- 8
            state.oamAddress <- state.oamAddress &&& 0xfcuy

      | 5 ->
        spPhase <- 6
        state.oamAddress <- state.oamAddress + 1uy

      | 6 ->
        spPhase <- 7
        state.oamAddress <- state.oamAddress + 1uy

      | 7 ->
        spPhase <- 8
        state.oamAddress <- state.oamAddress + 1uy

      | 8 ->
        state.oamAddress <- state.oamAddress + 4uy

      | _ ->
        ()


  let evaluationBegin state =
    state.oamAddress <- 0uy

    spCount <- 0
    spIndex <- 0
    spPhase <- 0


  let evaluationReset state =
    evaluationBegin state

    for i in 0 .. 255 do
      pixel.[i] <- 0
    

  let pointBit0 state =
    let sprite = spFound.[(state.h >>> 3) &&& 7]
    let mutable raster = state.v - (int sprite.Y)

    if ((sprite.Attr &&& Sprite.FlipV) <> 0) then
        raster <- raster ^^^ 0xf

    if state.objRasters = 8 then
      state.fetchAddress <- ((int sprite.Name) <<< 4) ||| (raster &&& 7) ||| state.objAddress
    else
      sprite.Name <- ((sprite.Name >>> 1) ||| (sprite.Name <<< 7)) &&& 0xff
      state.fetchAddress <- ((int sprite.Name) <<< 5) ||| (raster &&& 7) ||| ((raster <<< 1) &&& 0x10)

    state.fetchAddress <- state.fetchAddress ||| 0


  let pointBit1 state =
    state.fetchAddress <- state.fetchAddress ||| 8


  let fetchBit0 data state =
    let sprite = spFound.[(state.h >>> 3) &&& 7]

    if sprite.X = 255 || sprite.Y = 255 then
      state.fetchBit0 <- 0uy
    elif (sprite.Attr &&& Sprite.FlipH) <> 0 then
      state.fetchBit0 <- Byte.reverse (int data)
    else
      state.fetchBit0 <- data


  let fetchBit1 data state =
    let sprite = spFound.[(state.h >>> 3) &&& 7]

    if (sprite.X = 255 || sprite.Y = 255) then
      state.fetchBit1 <- 0uy
    else if (sprite.Attr &&& Sprite.FlipH) <> 0 then
      state.fetchBit1 <- Byte.reverse (int data)
    else
      state.fetchBit1 <- data


  let initializeSprite () =
    for i in 0 .. 7 do
      spFound.[i] <- Sprite.init ()

    spLatch <- 0
    spCount <- 0
    spIndex <- 0
    spPhase <- 0


  let resetSprite state =
    spLatch <- 0
    spCount <- 0
    spIndex <- 0
    spPhase <- 0

    state.oamAddress <- 0uy


  let getPixel state =
    if not state.objEnabled || (state.objClipped && state.h < 8) || state.h = 255 then
      0
    else
      pixel.[state.h]
