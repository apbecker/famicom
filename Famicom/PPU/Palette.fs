module Famicom.Palette

open System
open System.Linq
open Famicom.Platform.Util


let private wave (phase, color) =
  (color + phase + 8) % 12 < 6


let private clamp value =
  let mutable value = value
  value <- min value 255.0
  value <- max value 0.0

  byte value


let private gamma value =
  let gammaConstant = 2.2 / 1.8

  if value < 0.0 then
    0uy
  else
    clamp(255.0 * (value ** gammaConstant))


let private fromRGB (r: byte) (g: byte) (b: byte) =
  let a = 255uy in

  ((int a) <<< 24) |||
  ((int r) <<< 16) |||
  ((int g) <<<  8) |||
  ((int b) <<<  0)


let private generateColor pixel =
  let tau = Math.PI * 2.0

  let black = 0.518
  let white = 1.962
  let attenuation = 0.746

  let color = (pixel &&& 0x0f)
  let level = if (color < 0x0e) then (pixel >>> 4) &&& 3 else 1

  let levels = [|
    0.350
    0.518
    0.962
    1.550
    1.090
    1.500
    1.960
    1.960
  |]

  let loAndHi = [|
    levels.[level + if (color  = 0x0) then 4 else 0]
    levels.[level + if (color <= 0xc) then 4 else 0]
  |]

  let mutable y = 0.0
  let mutable i = 0.0
  let mutable q = 0.0

  for p in Enumerable.Range(0, 12) do
    let mutable spot = loAndHi.[if wave(p, color) then 1 else 0]

    if (((pixel &&& (1 <<< 6)) <> 0 && wave(p, 0)) ||
        ((pixel &&& (1 <<< 7)) <> 0 && wave(p, 4)) ||
        ((pixel &&& (1 <<< 8)) <> 0 && wave(p, 8))) then
      spot <- spot * attenuation

    let v = (spot - black) / (white - black)
    
    y <- y + (v / 12.0)
    i <- i + (v / 12.0) * cos((tau / 12.0) * (double p))
    q <- q + (v / 12.0) * sin((tau / 12.0) * (double p))

  let r = gamma(y + 0.946882 * i + 0.623557 * q)
  let g = gamma(y - 0.274788 * i - 0.635691 * q)
  let b = gamma(y - 1.108545 * i + 1.709007 * q)

  fromRGB r g b


let private lookup =
  generateColor
    |> Seq.init (64 * 8)
    |> Seq.toArray


let color i =
  lookup.[i]
