namespace Famicom.PPU

open Famicom.Memory

type R2C02State =
  { mutable cycles: int
    mutable field: bool
    mutable objOverflow: bool
    mutable objZeroHit: bool
    mutable vblEnabled: bool
    mutable vblFlag: bool
    mutable vblHold: bool
    mutable h: int
    mutable v: int
    mutable clipping: int
    mutable emphasis: int
    mutable chr: byte
    mutable oamAddress: byte
    oam: byte[]

    cgram: Memory

    mutable bkgClipped: bool
    mutable bkgEnabled: bool
    mutable bkgAddress: int

    mutable objClipped: bool
    mutable objEnabled: bool
    mutable objAddress: int
    mutable objRasters: int

    scroll: Scroll

    mutable fetchAttr: byte
    mutable fetchBit0: byte
    mutable fetchBit1: byte
    mutable fetchName: byte
    mutable fetchAddress: int }

module R2C02State =

  let init () =
    { cycles = 0
      field = false
      objOverflow = false
      objZeroHit = false
      vblEnabled = false
      vblFlag = false
      vblHold = false
      h = 0
      v = 261
      clipping = 0x3f
      emphasis = 0
      chr = 0uy
      oamAddress = 0uy
      oam = Array.zeroCreate<byte> 256

      cgram = CGRAM.init ()

      bkgClipped = true
      bkgEnabled = false
      bkgAddress = 0

      objClipped = true
      objEnabled = false
      objAddress = 0
      objRasters = 8

      scroll = Scroll.init ()

      fetchAttr = 0uy
      fetchBit0 = 0uy
      fetchBit1 = 0uy
      fetchName = 0uy
      fetchAddress = 0 }
