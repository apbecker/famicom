module Famicom.PPU.R2C02MemoryMap

open Famicom.Memory


let vram = Memory.ram 0x800


let vramAddress address a10 =
  (a10 <<< 10) ||| (address &&& 0x3ff)


let read cartridgeIO ciram address data =
  let data = cartridgeIO address |> ValueOption.defaultValue data

  if ((address &&& 0x3fff) <= 0x1fff) then
    data
  else
    ciram address
      |> ValueOption.map (vramAddress address)
      |> ValueOption.map (Memory.read vram)
      |> ValueOption.defaultValue data


let write cartridgeIO ciram address data =
  cartridgeIO address data

  if (address &&& 0x3fff) <= 0x1fff then
    ()
  else
    ciram address
      |> ValueOption.map (vramAddress address)
      |> ValueOption.iter (Memory.write vram data)
