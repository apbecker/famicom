module Famicom.PPU.R2C02Registers

open Famicom.Memory


let private vbl sendVBL state: unit =
  sendVBL (state.vblEnabled && state.vblFlag)


let private read2002 sendVBL state data =
  let mutable data = int data
  data <- data &&& 0x1f

  if (state.vblFlag) then data <- data ||| 0x80
  if (state.objZeroHit) then data <- data ||| 0x40
  if (state.objOverflow) then data <- data ||| 0x20

  state.vblHold <- false
  state.vblFlag <- false
  state.scroll.swap <- false

  vbl sendVBL state

  byte data


let private read2004 state =
  state.oam.[int state.oamAddress]


let private read2007 read state =
  let data =
    if (state.scroll.address &&& 0x3f00) = 0x3f00 then
      CGRAM.read state.cgram state.scroll.address
    else
      state.chr

  state.chr <- read state.scroll.address state.chr
  state.scroll.address <- (state.scroll.address + state.scroll.step) &&& 0x7fff

  data


let read sendVBL read state address data =
  match address &&& 0x2007 with
  | 0x2002 -> read2002 sendVBL state data
  | 0x2004 -> read2004 state
  | 0x2007 -> read2007 read state
  | _ ->
    data


let private write2000 sendVBL state data =
  state.scroll.latch <- (state.scroll.latch &&& 0x73ff) ||| ((data <<< 10) &&& 0x0c00)
  state.scroll.step <- if (data &&& 0x04) <> 0 then 0x0020 else 0x0001
  state.objAddress <- if (data &&& 0x08) <> 0 then 0x1000 else 0x0000
  state.bkgAddress <- if (data &&& 0x10) <> 0 then 0x1000 else 0x0000
  state.objRasters <- if (data &&& 0x20) <> 0 then 0x0010 else 0x0008
  state.vblEnabled <- (data &&& 0x80) <> 0

  vbl sendVBL state


let private write2001 state data =
  state.bkgClipped <- (data &&& 0x02)  = 0
  state.objClipped <- (data &&& 0x04)  = 0
  state.bkgEnabled <- (data &&& 0x08) <> 0
  state.objEnabled <- (data &&& 0x10) <> 0

  state.clipping <- if (data &&& 0x01) <> 0 then 0x30 else 0x3f
  state.emphasis <- (data &&& 0xe0) <<< 1


let private write2003 state data =
  state.oamAddress <- byte data


let private write2004 state data =
  state.oam.[int state.oamAddress] <- byte <|
    if (state.oamAddress &&& 3uy) = 2uy then
      data &&& 0xe3
    else
      data

  state.oamAddress <- state.oamAddress + 1uy


let private write2005 state data =
  state.scroll.swap <- not state.scroll.swap
  if state.scroll.swap then
    state.scroll.latch <- (state.scroll.latch &&& ~~~0x001f) ||| ((data &&& ~~~7) >>> 3)
    state.scroll.fine <- (data &&& 0x07)
  else
    state.scroll.latch <- (state.scroll.latch &&& ~~~0x73e0) ||| ((data &&& 7) <<< 12) ||| ((data &&& ~~~7) <<< 2)


let private write2006 read state data =
  state.scroll.swap <- not state.scroll.swap
  if state.scroll.swap then
    state.scroll.latch <- (state.scroll.latch &&& ~~~0xff00) ||| ((data &&& 0x3f) <<< 8)
  else
    state.scroll.latch <- (state.scroll.latch &&& ~~~0x00ff) ||| ((data &&& 0xff) <<< 0)
    state.scroll.address <- state.scroll.latch

    read state.scroll.address (byte data)
      |> ignore


let private write2007 write state data =
  if (state.scroll.address &&& 0x3f00) = 0x3f00 then
    state.scroll.address |> CGRAM.write state.cgram (byte data)
  else
    write state.scroll.address (byte data)

  state.scroll.address <- (state.scroll.address + state.scroll.step) &&& 0x7fff


let write sendVBL read write state address data =
  match address &&& 0x2007 with
  | 0x2000 -> write2000 sendVBL state data
  | 0x2001 -> write2001 state data
  | 0x2002 -> ()
  | 0x2003 -> write2003 state data
  | 0x2004 -> write2004 state data
  | 0x2005 -> write2005 state data
  | 0x2006 -> write2006 read state data
  | 0x2007 -> write2007 write state data
  | _ ->
    ()
