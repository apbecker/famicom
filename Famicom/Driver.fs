module Famicom.Driver

open Famicom.Boards
open Famicom.CPU
open Famicom.Input
open Famicom.PPU
open Famicom.Platform.Hosting


let create = DriverFactory (fun binary ->
  let board =
    match CartridgeBoard.tryInit binary with
    | Ok(board) -> board
    | Error(msg) -> failwith msg

  let input =
    InputConnector.init
      <| Joypad.create 0
      <| Joypad.create 1

  let r2c02State = R2C02State.init ()
  let r2a03State = R2A03State.init ()

  let sendHalfFrame () = R2A03.halfFrame r2a03State
  let sendQuadFrame () = R2A03.quadFrame r2a03State
  let sendIRQ = R2A03.irq r2a03State
  let sendNMI = R2A03.nmi r2a03State

  let readPPU =
    R2C02MemoryMap.read
      (CartridgeBoard.readPPU board)
      (CartridgeBoard.ciram board)

  let writePPU =
    R2C02MemoryMap.write
      (CartridgeBoard.writePPU board)
      (CartridgeBoard.ciram board)

  let readCPU =
    R2A03MemoryMap.read
      (CartridgeBoard.readCPU board)
      (R2A03Registers.read input sendIRQ r2a03State)
      (R2C02Registers.read sendNMI readPPU r2c02State)

  let writeCPU =
    R2A03MemoryMap.write
      (CartridgeBoard.writeCPU board)
      (R2A03Registers.write input sendIRQ sendHalfFrame sendQuadFrame r2a03State)
      (R2C02Registers.write sendNMI readPPU writePPU r2c02State)

  r2a03State.core <-
    RP6502.Core.init
      (fun () -> r2a03State.cyclesForThisInstruction <- r2a03State.cyclesForThisInstruction + 132)
      (fun address data -> readCPU (int address) data)
      (fun address data -> writeCPU (int address) data)

  let clockCPU audio = R2A03.clock audio
  let clockPPU video = R2C02.clock video sendNMI (fun address -> readPPU address 0uy)

  R2A03.resetHard r2a03State
  R2C02.init r2c02State

  let mutable cycles = 0

  let runForOneFrame audio video =
    let cyclesPerSecond = 236250000 // 1.789~ × 132

    InputConnector.frame input

    while cycles < cyclesPerSecond do
      let adding = R2A03.update r2a03State

      adding |> clockCPU audio r2a03State
      adding |> clockPPU video r2c02State

      cycles <- cycles + adding * 60

    cycles <- cycles - cyclesPerSecond
  in
  Driver(runForOneFrame)
)


let definition =
  { name = "Famicom"
    extensions = [ ".nes" ]
    audio =
      { backend = "sdl2"
        channels = 1
        sampleRate = 48000 }
    video =
      { backend = "sdl2"
        width = 256
        height = 240 }
    factory = create }
