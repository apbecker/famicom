module Famicom.Platform.Audio.NullAudio

open Famicom.Platform.Hosting


let create (_definition: AudioDefinition) =
  ()


let destroy () =
  ()


let sample (_sample: int) =
  ()


let render () =
  ()
