namespace Famicom.Platform.Audio

type AudioBackend =
  | Null
  | SDL2 of SDL2Audio

module AudioBackend =

  open Famicom.Platform.Hosting


  let create (definition: AudioDefinition) =
    match definition.backend with
    | "sdl2" ->
      SDL2 <| SDL2Audio.create definition

    | _ ->
      Null


  let destroy audio =
    match audio with
    | Null ->
      NullAudio.destroy ()

    | SDL2 sdl2 ->
      SDL2Audio.destroy sdl2


  let getSink audio =
    let sink =
      match audio with
      | Null ->
        NullAudio.sample

      | SDL2 sdl2 ->
        SDL2Audio.sample sdl2
    in
    AudioSink(sink)


  let render audio =
    match audio with
    | Null ->
      NullAudio.render ()

    | SDL2 sdl2 ->
      SDL2Audio.render sdl2
