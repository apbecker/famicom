module Famicom.Platform.Hosting

type VideoStatus =
  | Continue
  | Exit


type AudioSink =
  | AudioSink of (int -> unit)

module AudioSink =

  let run (AudioSink f) sample =
    f sample


type VideoSink =
  | VideoSink of (int -> int -> int -> unit)

module VideoSink =

  let run (VideoSink f) x y color =
    f x y color


type Driver =
  | Driver of (AudioSink -> VideoSink -> unit)


type DriverFactory =
  | DriverFactory of (byte [] -> Driver)


type AudioDefinition =
  { backend: string
    channels: int
    sampleRate: int }


type VideoDefinition =
  { backend: string
    width: int
    height: int }


type DriverDefinition =
  { name: string
    extensions: List<string>
    audio: AudioDefinition
    video: VideoDefinition
    factory: DriverFactory }
