[<RequireQualifiedAccess>]
module Famicom.Platform.DriverHost

open Famicom.Platform.Hosting
open Famicom.Platform.Audio
open Famicom.Platform.Video


let main (Driver driver) audio video =
  let audioSink = AudioBackend.getSink audio in
  let videoSink = VideoBackend.getSink video in

  let rec loop =
    function
    | Exit ->
      ()

    | Continue ->
      driver audioSink videoSink
      AudioBackend.render audio
      VideoBackend.render video |> loop
  in
  loop Continue
