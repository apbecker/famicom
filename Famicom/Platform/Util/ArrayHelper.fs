[<RequireQualifiedAccess>]
module Famicom.Platform.Util.ArrayHelper

let create1 factory length =
  factory
    |> Seq.init length
    |> Seq.toArray
  

let create2 factory length1 length2 =
  length1 |> create1 (fun i ->
  length2 |> create1 (fun j -> factory i j))


let fill2 length1 length2 value =
  create2 (fun _ _ -> value) length1 length2
