namespace Famicom.Platform.Video

#nowarn "9"

type SDL2Video =
  { width: int
    height: int
    window: nativeint
    renderer: nativeint
    texture: nativeint
    screen: int[][] }

module SDL2Video =

  open Famicom.Platform.Hosting
  open Famicom.Platform.Util
  open Microsoft.FSharp.NativeInterop
  open SDL2


  let create (config: VideoDefinition) =
    let width = config.width in
    let height = config.height in
    let screen = ArrayHelper.fill2 height width 0 in

    SDL.SDL_Init SDL.SDL_INIT_VIDEO |> ignore

    let window =
      SDL.SDL_CreateWindow
        ( "SDL2"
        , SDL.SDL_WINDOWPOS_CENTERED
        , SDL.SDL_WINDOWPOS_CENTERED
        , width
        , height
        , SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN )

    let flags =
        SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED |||
        SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC

    let renderer =
      SDL.SDL_CreateRenderer
        ( window
        , -1
        , flags )

    let texture =
      SDL.SDL_CreateTexture
        ( renderer
        , SDL.SDL_PIXELFORMAT_ARGB8888
        , int SDL.SDL_TextureAccess.SDL_TEXTUREACCESS_STREAMING
        , width
        , height )

    { width = width
      height = height
      window = window
      renderer = renderer
      texture = texture
      screen = screen }


  let destroy sdl2 =
    SDL.SDL_DestroyTexture(sdl2.texture)
    SDL.SDL_DestroyRenderer(sdl2.renderer)
    SDL.SDL_DestroyWindow(sdl2.window)


  let private getEvents () =
    let mutable event = Unchecked.defaultof<SDL.SDL_Event> in
    seq {
      while SDL.SDL_PollEvent (&event) = 1 do
        yield event
    }


  let private processEvents =
    let picker (event: SDL.SDL_Event) =
      if event.``type`` = SDL.SDL_EventType.SDL_QUIT then
        Some Exit
      else
        None
    in
    getEvents
      >> Seq.tryPick picker
      >> Option.defaultValue Continue


  let render sdl2 =
    let nullptr = nativeint 0 in

    match SDL.SDL_LockTexture(sdl2.texture, nullptr) with
    | (0, pixels, pitch) ->
      let mutable pixels =
        NativePtr.ofNativeInt pixels

      for y in 0 .. (sdl2.height - 1) do
        for x in 0 .. (sdl2.width - 1) do
          NativePtr.set pixels x (Array.get (Array.get sdl2.screen y) x)

        pixels <- NativePtr.add pixels (pitch / sizeof<int>)

      SDL.SDL_UnlockTexture(sdl2.texture)
      SDL.SDL_RenderCopy(sdl2.renderer, sdl2.texture, nullptr, nullptr) |> ignore
      SDL.SDL_RenderPresent(sdl2.renderer)

      processEvents ()

    | _ ->
      Exit


  let sink sdl2 x y color =
    sdl2.screen.[y].[x] <- color
