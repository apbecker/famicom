namespace Famicom.Platform.Video

type VideoBackend =
  | Null
  | SDL2 of SDL2Video

module VideoBackend =

  open Famicom.Platform.Hosting


  let create (definition: VideoDefinition) =
    match definition.backend with
    | "sdl2" ->
      SDL2 <| SDL2Video.create definition

    | _ ->
      Null


  let destroy video =
    match video with
    | Null ->
      NullVideo.destroy ()

    | SDL2 sdl2 ->
      SDL2Video.destroy sdl2


  let getSink video =
    let sink =
      match video with
      | Null ->
        NullVideo.sink

      | SDL2 sdl2 ->
        SDL2Video.sink sdl2
    in
    VideoSink(sink)


  let render video =
    match video with
    | Null ->
      NullVideo.render ()

    | SDL2 sdl2 ->
      SDL2Video.render sdl2
