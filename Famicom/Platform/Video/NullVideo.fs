module Famicom.Platform.Video.NullVideo

open Famicom.Platform.Hosting


let create (_definition: VideoDefinition) =
  ()


let destroy () =
  ()


let sink (_x: int) (_y: int) (_color: int) =
  ()


let render () =
  Continue
