namespace Famicom.Platform.Input

type InputBackend (_index: int, _numberOfButtons: int) =

  member __.Map (_index: int) (_button: string) = ()
  member __.Pressed(_index: int) = false
  member __.Update () = ()
