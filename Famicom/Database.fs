namespace Famicom

type Database = FSharp.Data.XmlProvider<Schema = "db.xsd">

module Database =

  open System.IO
  open System.Reflection
  open System.Security.Cryptography


  [<Literal>]
  let private HeaderSize = 16


  let private getHash data =
    SHA1.Create().ComputeHash(data, HeaderSize, data.Length - HeaderSize)
      |> Seq.map (sprintf "%02x")
      |> String.concat ""


  let private boards =
    let basePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
    use stream = File.OpenRead(Path.Combine(basePath, "fc", "db.xml"))
    let database = Database.Load(stream)

    Map.ofSeq <| seq {
      for game in database.Games do
        for cartridge in game.Cartridges do
          yield (cartridge.Sha1, cartridge.Board) }


  let tryFind romData =
    let hash = getHash romData

    printfn "Looking up image with hash %s .." hash

    Map.tryFind hash boards
