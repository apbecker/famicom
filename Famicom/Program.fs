module Famicom.Program

open System.IO
open Famicom.Platform
open Famicom.Platform.Hosting
open Famicom.Platform.Audio
open Famicom.Platform.Video


let private runGame fileName =
  let binary = File.ReadAllBytes(fileName)

  let definition = Driver.definition
  let (DriverFactory(factory)) = definition.factory
  let driver = factory binary
  let audio = AudioBackend.create definition.audio
  let video = VideoBackend.create definition.video

  DriverHost.main driver audio video


[<EntryPoint>]
let main args =
  match args with
  | [| fileName |] ->
    runGame fileName

  | _ ->
    printfn "Wrong number of parameters"
  
  0
