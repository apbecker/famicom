namespace Famicom.CPU

open Famicom.CPU.Audio


type R2A03State =
  { mutable core: RP6502.Core
    mutable cyclesForThisInstruction: int
    mutable sequenceIrqEnabled: bool
    mutable sequenceIrqPending: bool
    mutable sequenceTime: int
    mutable sequenceMode: int

    mutable samplePrescaler: int

    mutable dmaTrigger: bool
    mutable dmaSegment: byte

    sq1: Square
    sq2: Square
    tri: Triangle
    noi: Noise
    dmc: Dmc }

module R2A03State =

  let init () =
    { core = Unchecked.defaultof<_>
      cyclesForThisInstruction = 0
      sequenceIrqEnabled = true
      sequenceIrqPending = false
      sequenceTime = 0
      sequenceMode = 0

      samplePrescaler = 19687500

      dmaTrigger = false
      dmaSegment = 0uy

      sq1 = Square.init 0
      sq2 = Square.init 1
      tri = Triangle.init ()
      noi = Noise.init ()
      dmc = Dmc.init () }
