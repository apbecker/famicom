module Famicom.CPU.R2A03MemoryMap

open Famicom.Memory


let wram = Memory.ram 0x800


let read cartridgeIO cpuIO ppuIO address data =
  let data = cartridgeIO address |> ValueOption.defaultValue data

  match address with
  | address when address <= 0x1fff -> Memory.read wram address
  | address when address <= 0x3fff -> ppuIO address data
  | address when address <= 0x4017 -> cpuIO address data
  | _ ->
    data


let write cartridgeIO cpuIO ppuIO address data =
  cartridgeIO address data

  match address with
  | address when address <= 0x1fff -> Memory.write wram data address
  | address when address <= 0x3fff -> ppuIO address (int data)
  | address when address <= 0x4017 -> cpuIO address (int data)
  | _ ->
    ()
