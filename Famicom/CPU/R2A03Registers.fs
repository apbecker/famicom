module Famicom.CPU.R2A03Registers

open Famicom.CPU.Audio
open Famicom.Input


let read (input: InputConnector) sendIRQ r2a03 address data =
  match address with
  | 0x4015 ->
    let data = (byte)(
      (if r2a03.sq1.duration.counter <> 0 then 0x01 else 0) |||
      (if r2a03.sq2.duration.counter <> 0 then 0x02 else 0) |||
      (if r2a03.tri.duration.counter <> 0 then 0x04 else 0) |||
      (if r2a03.noi.duration.counter <> 0 then 0x08 else 0) |||
      (if r2a03.sequenceIrqPending then 0x40 else 0))

    r2a03.sequenceIrqPending <- false
    sendIRQ false
    data

  | 0x4016 ->
    (data &&& 0xe0uy) ||| (InputConnector.readJoypad1 input)

  | 0x4017 ->
    (data &&& 0xe0uy) ||| (InputConnector.readJoypad2 input)

  | _ ->
    data


let write (input: InputConnector) sendIRQ sendHalfFrame sendQuadFrame r2a03 address data =
  match address &&& ~~~3 with
  | 0x4000 -> Square.write r2a03.sq1 (address - 0x4000) data
  | 0x4004 -> Square.write r2a03.sq2 (address - 0x4004) data
  | 0x4008 -> Triangle.write r2a03.tri (address - 0x4008) data
  | 0x400c -> Noise.write r2a03.noi (address - 0x400c) data
  | 0x4010 -> Dmc.write r2a03.dmc (address - 0x4010) data
  | _ ->
    match address with
    | 0x4014 ->
      r2a03.dmaSegment <- byte data
      r2a03.dmaTrigger <- true

    | 0x4015 ->
      r2a03.sq1.enabled <- (data &&& 0x01) <> 0
      r2a03.sq2.enabled <- (data &&& 0x02) <> 0
      r2a03.tri.enabled <- (data &&& 0x04) <> 0
      r2a03.noi.enabled <- (data &&& 0x08) <> 0

      if (not r2a03.sq1.enabled) then r2a03.sq1.duration.counter <- 0
      if (not r2a03.sq2.enabled) then r2a03.sq2.duration.counter <- 0
      if (not r2a03.tri.enabled) then r2a03.tri.duration.counter <- 0
      if (not r2a03.noi.enabled) then r2a03.noi.duration.counter <- 0

    | 0x4016 ->
      InputConnector.write input (byte data)

    | 0x4017 ->
      r2a03.sequenceIrqEnabled <- (data &&& 0x40) = 0

      if not r2a03.sequenceIrqEnabled then
        r2a03.sequenceIrqPending <- false
        sendIRQ false

      r2a03.sequenceMode <- (data >>> 7) &&& 1
      r2a03.sequenceTime <- 0

      if r2a03.sequenceMode = 1 then
        sendHalfFrame ()
        sendQuadFrame ()

    | _ ->
      ()
