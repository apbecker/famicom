namespace Famicom.CPU.Audio

type Square =
  { mutable enabled: bool

    divider: Divider
    duration: Duration
    envelope: Envelope
    sweep: Sweep

    mutable dutyForm: int
    mutable dutyStep: int }

module Square =

  let init sweepCarry =
    { enabled = false

      divider = Divider.init 0
      duration = Duration.init ()
      envelope = Envelope.init ()
      sweep = Sweep.init sweepCarry

      dutyForm = 0
      dutyStep = 0 }


  let tick square =
    if Divider.tick square.divider then
      square.dutyStep <- (square.dutyStep - 1) &&& 7


  let write square port data =
    match port with
    | 0 ->
      square.dutyForm <- (data >>> 6) &&& 3
      square.duration.halted <- (data &&& 0x20) <> 0
      square.envelope |> Envelope.control data

    | 1 ->
      square.sweep.enabled <- (data &&& 0x80) <> 0
      square.sweep.period <- (data >>> 4) &&& 7
      square.sweep.negated <- (data &&& 0x08) <> 0
      square.sweep.shift <- (data >>> 0) &&& 7
      square.sweep.reload <- true

    | 2 ->
      square.divider.period <- (square.divider.period &&& 0x700) ||| ((data <<< 0) &&& 0x0ff)

    | 3 ->
      square.divider.period <- (square.divider.period &&& 0x0ff) ||| ((data <<< 8) &&& 0x700)
      square.dutyStep <- 0
      square.envelope |> Envelope.reload

      if square.enabled then
        square.duration.counter <- Duration.decode (data >>> 3)

    | _ ->
      ()


  let private table = [|
    [| 0; 1; 0; 0; 0; 0; 0; 0 |]
    [| 0; 1; 1; 0; 0; 0; 0; 0 |]
    [| 0; 1; 1; 1; 1; 0; 0; 0 |]
    [| 1; 0; 0; 1; 1; 1; 1; 1 |]
  |]


  let output square =
    if square.divider.period < 8 || (square.sweep.target &&& 0x800) <> 0 then
      0
    else
      if square.duration.counter <> 0 && table.[square.dutyForm].[square.dutyStep] = 1 then
        Envelope.volume(square.envelope)
      else
        0
