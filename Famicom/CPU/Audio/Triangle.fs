namespace Famicom.CPU.Audio

type Triangle =
  { mutable enabled: bool

    divider: Divider
    duration: Duration

    mutable step: int

    mutable linearCounter: int
    mutable linearCounterLatch: int
    mutable linearCounterControl: bool
    mutable linearCounterReload: bool }

module Triangle =

  let init () =
    { enabled = false

      divider = Divider.init 0
      duration = Duration.init ()

      step = 0

      linearCounter = 0
      linearCounterLatch = 0
      linearCounterControl = false
      linearCounterReload = false }


  let tick triangle =
    if Divider.tick triangle.divider then
      if triangle.duration.counter <> 0 && triangle.linearCounter <> 0 then
        triangle.step <- (triangle.step + 1) &&& 31


  let write triangle port data =
    match port with
    | 0 ->
      triangle.duration.halted <- (data &&& 0x80) <> 0
      triangle.linearCounterControl <- (data &&& 0x80) <> 0
      triangle.linearCounterLatch <- (data &&& 0x7f)

    | 1 ->
      ()

    | 2 ->
      triangle.divider.period <- (triangle.divider.period &&& 0x700) ||| ((data <<< 0) &&& 0x0ff)

    | 3 ->
      triangle.divider.period <- (triangle.divider.period &&& 0x0ff) ||| ((data <<< 8) &&& 0x700)
      triangle.linearCounterReload <- true

      if triangle.enabled then
        triangle.duration.counter <- Duration.decode (data >>> 3)

    | _ ->
      ()

        
  let private table = [|
    0xf; 0xe; 0xd; 0xc; 0xb; 0xa; 0x9; 0x8; 0x7; 0x6; 0x5; 0x4; 0x3; 0x2; 0x1; 0x0
    0x0; 0x1; 0x2; 0x3; 0x4; 0x5; 0x6; 0x7; 0x8; 0x9; 0xa; 0xb; 0xc; 0xd; 0xe; 0xf
  |]


  let output triangle =
    if triangle.divider.period = 0 || triangle.divider.period = 1 then
      7
    else
      table.[triangle.step]
