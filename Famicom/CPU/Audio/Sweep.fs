namespace Famicom.CPU.Audio

type Sweep =
  { carry: int
    mutable enabled: bool
    mutable period: int
    mutable timer: int
    mutable negated: bool
    mutable reload: bool
    mutable shift: int
    mutable target: int }

module Sweep =

  let init carry =
    { carry = carry
      enabled = false
      period = 0
      timer = 0
      negated = false
      reload = false
      shift = 0
      target = 0 }


  let tick sweep period =
    if sweep.reload then
      sweep.reload <- false
      sweep.timer <- sweep.period
      period
    else
      if sweep.timer <> 0 then
        sweep.timer <- sweep.timer - 1
        period
      else
        sweep.timer <- sweep.period

        if sweep.enabled && sweep.shift <> 0 then
          sweep.target <-
            if sweep.negated then
              period + ((~~~period + sweep.carry) >>> sweep.shift)
            else
              period + (period >>> sweep.shift)

          if sweep.target >= 0 && sweep.target <= 0x7ff then
            sweep.target
          else
            period
        else
          period
