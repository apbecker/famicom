module Famicom.CPU.Audio.Mixer

open Famicom.CPU
open Famicom.Platform.Hosting


let private sampleStep = 48000 * 11


let private samplePeriod = 19687500 // 1,789,772.72~ * 11


let private mixSamples sq1 sq2 tri noi dmc =
  let sqrBase = 95.52
  let sqrDiv = 8128.0

  let tndBase = 163.67
  let tndDiv = 24329.0

  let sqrN = double (sq1 + sq2)
  let sqr = sqrBase / (sqrDiv / sqrN + 100.0)

  let tndN = double (tri * 3 + noi * 2 + dmc)
  let tnd = tndBase / (tndDiv / tndN + 100.0)

  int ((sqr + tnd) * 32767.0)


let private sample r2a03 audio =
  let sq1 = Square.output r2a03.sq1
  let sq2 = Square.output r2a03.sq2
  let tri = Triangle.output r2a03.tri
  let noi = Noise.output r2a03.noi
  let dmc = Dmc.output r2a03.dmc

  mixSamples sq1 sq2 tri noi dmc
    |> AudioSink.run audio


let tick r2a03 audio =
  r2a03.samplePrescaler <- r2a03.samplePrescaler + sampleStep

  if r2a03.samplePrescaler >= samplePeriod then
    r2a03.samplePrescaler <- r2a03.samplePrescaler - samplePeriod
    sample r2a03 audio
