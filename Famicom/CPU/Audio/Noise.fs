namespace Famicom.CPU.Audio

type Noise =
  { mutable enabled: bool

    divider: Divider
    duration: Duration
    envelope: Envelope

    mutable lfsrMode: int
    mutable lfsr: int }

module Noise =

  let init () =
    { enabled = false

      divider = Divider.init 0
      duration = Duration.init ()
      envelope = Envelope.init ()

      lfsrMode = 0
      lfsr = 1 }


  let tick noise =
    if Divider.tick noise.divider then
      let tap0 = noise.lfsr
      let tap1 =
        if noise.lfsrMode = 1 then
          noise.lfsr >>> 6
        else
          noise.lfsr >>> 1

      let feedback = (tap0 ^^^ tap1) &&& 1

      noise.lfsr <- (noise.lfsr >>> 1) ||| (feedback <<< 14)


  let private periodTable = [|
    4
    8
    16
    32
    64
    96
    128
    160
    202
    254
    380
    508
    762
    1016
    2034
    4068
  |]


  let write noise port data =
    match port with
    | 0 ->
      noise.duration.halted <- (data &&& 0x20) <> 0
      noise.envelope |> Envelope.control data

    | 1 ->
      ()

    | 2 ->
      noise.lfsrMode <- (data >>> 7) &&& 1
      noise.divider.period <- periodTable.[data &&& 15]

    | 3 ->
      noise.envelope |> Envelope.reload

      if noise.enabled then
        noise.duration.counter <- Duration.decode (data >>> 3)

    | _ ->
      ()


  let output noise =
    if noise.duration.counter <> 0 && (noise.lfsr &&& 1) = 0 then
      Envelope.volume(noise.envelope)
    else
      0
