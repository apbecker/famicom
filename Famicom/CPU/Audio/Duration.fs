namespace Famicom.CPU.Audio

type Duration =
  { mutable halted: bool
    mutable counter: int
    mutable latch: int }

module Duration =

  let private table = [|
    10;254; 20;  2; 40;  4; 80;  6; 160;  8; 60; 10; 14; 12; 26; 14
    12; 16; 24; 18; 48; 20; 96; 22; 192; 24; 72; 26; 16; 28; 32; 30
  |]


  let init () =
    { halted = false
      counter = 0
      latch = 0 }


  let tick duration =
    if duration.counter <> 0 && not duration.halted then
      duration.counter <- duration.counter - 1


  let decode index =
    Array.get table index
