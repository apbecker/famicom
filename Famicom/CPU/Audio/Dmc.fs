namespace Famicom.CPU.Audio

type Dmc = unit

module Dmc =

  let init () =
    ()


  let write (_dmc: Dmc) port data =
    ()


  let output (_dmc: Dmc) =
    0
