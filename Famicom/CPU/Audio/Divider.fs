namespace Famicom.CPU.Audio

type Divider =
  { mutable counter: int
    mutable period: int }

module Divider =

  let init period =
    { counter = period
      period = period }


  let tick divider =
    if divider.counter = 0 then
      divider.counter <- divider.period
      true
    else
      divider.counter <- divider.counter - 1
      false


  let reload divider =
    divider.counter <- divider.period


  let tickMany inputs divider =
    if divider.counter >= inputs then
      divider.counter <- divider.counter - inputs
      0
    else
      let inputs' = inputs - (divider.counter + 1)

      let q = inputs' / (divider.period + 1)
      let r = inputs' % (divider.period + 1)

      divider.counter <- divider.period - r
      1 + q
