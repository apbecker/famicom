namespace Famicom.CPU.Audio

type Envelope =
  { divider: Divider
    mutable constant: bool
    mutable looping: bool
    mutable start: bool
    mutable decay: int }

module Envelope =

  let init () =
    { divider = Divider.init 0
      constant = false
      looping = false
      start = false
      decay = 0 }


  let tick envelope =
    if envelope.start then
      envelope.start <- false
      envelope.decay <- 15
      envelope.divider |> Divider.reload
    else
      if Divider.tick envelope.divider then
        if envelope.decay <> 0 then
          envelope.decay <- envelope.decay - 1
        elif envelope.looping then
          envelope.decay <- 15


  let volume envelope =
    if envelope.constant then
      envelope.divider.period
    else
      envelope.decay


  let reload envelope =
    envelope.start <- true


  let control value envelope =
    envelope.looping <- (value &&& 0x20) = 0
    envelope.constant <- (value &&& 0x10) <> 0
    envelope.divider.period <- value &&& 15
