module Famicom.CPU.R2A03

open System
open Famicom.CPU.Audio
open Famicom.CPU.RP6502


let runDma r2a03 =
  let mutable dmaSrcAddress = uint16 ((int r2a03.dmaSegment) <<< 8)
  let dmaDstAddress = 0x2004us

  for _ in 0 .. 255 do
    let data = IO.read dmaSrcAddress r2a03.core
    IO.write dmaDstAddress data r2a03.core

    dmaSrcAddress <- dmaSrcAddress + 1us


let update r2a03 =
  r2a03.cyclesForThisInstruction <- 0
  r2a03.core |> Core.step

  if r2a03.dmaTrigger then
    r2a03.dmaTrigger <- false

    runDma r2a03

  r2a03.cyclesForThisInstruction


let sequencerInterrupt r2a03 =
  r2a03.sequenceIrqPending <- r2a03.sequenceIrqPending || r2a03.sequenceIrqEnabled

  if r2a03.sequenceIrqPending then
    r2a03.core |> Core.irq true


let halfFrame r2a03 =
  let sq1 = r2a03.sq1
  let sq2 = r2a03.sq2
  let tri = r2a03.tri
  let noi = r2a03.noi

  Duration.tick sq1.duration
  Duration.tick sq2.duration
  Duration.tick tri.duration
  Duration.tick noi.duration

  sq1.divider.period <- Sweep.tick sq1.sweep sq1.divider.period
  sq2.divider.period <- Sweep.tick sq2.sweep sq2.divider.period


let quadFrame r2a03 =
  Envelope.tick(r2a03.sq1.envelope)
  Envelope.tick(r2a03.sq2.envelope)
  Envelope.tick(r2a03.noi.envelope)

  let tri = r2a03.tri
  if tri.linearCounterReload then
    tri.linearCounter <- tri.linearCounterLatch
  else
    if tri.linearCounter <> 0 then
      tri.linearCounter <- tri.linearCounter - 1

  if not tri.linearCounterControl then
    tri.linearCounterReload <- false


let tickAudio audio r2a03 =
  if r2a03.sequenceMode = 0 then
    match r2a03.sequenceTime with
    |     0 ->
      sequencerInterrupt r2a03

    |  7457 ->
      quadFrame r2a03

    | 14913 ->
      quadFrame r2a03
      halfFrame r2a03

    | 22371 ->
      quadFrame r2a03

    | 29828 ->
      sequencerInterrupt r2a03

    | 29829 ->
      quadFrame r2a03
      halfFrame r2a03
      sequencerInterrupt r2a03

    | _ ->
      ()

    r2a03.sequenceTime <- r2a03.sequenceTime + 1
    if r2a03.sequenceTime = 29830 then
      r2a03.sequenceTime <- 0
  else
    match r2a03.sequenceTime with
    |  7457 ->
      quadFrame r2a03

    | 14913 ->
      quadFrame r2a03
      halfFrame r2a03

    | 22371 ->
      quadFrame r2a03

    | 37281 ->
      quadFrame r2a03
      halfFrame r2a03

    | _ ->
      ()

    r2a03.sequenceTime <- r2a03.sequenceTime + 1
    if r2a03.sequenceTime = 37282 then
      r2a03.sequenceTime <- 0

  Square.tick r2a03.sq1
  Square.tick r2a03.sq2
  Triangle.tick r2a03.tri
  Noise.tick r2a03.noi
  Mixer.tick r2a03 audio


let clock audio r2a03 amount =
  for _ in 0 .. 132 .. amount do
    tickAudio audio r2a03


let irq r2a03 value = r2a03.core |> Core.irq value


let nmi r2a03 value = r2a03.core |> Core.nmi value


let resetHard r2a03 = r2a03.core |> Core.reset true
