namespace Famicom.CPU.RP6502

type Core =
  { tick: unit -> unit
    read: uint16 -> uint8 -> uint8
    write: uint16 -> uint8 -> unit
    interrupts: Interrupts
    registers: Registers
    status: Status
    mutable dataBus: uint8 }


type Implied = Implied of (Core -> unit)


type Reader = Reader of (uint8 -> Core -> unit)


type Modifier = Modifier of (uint8 -> Core -> uint8)


type Writer = Writer of (Core -> uint8)
