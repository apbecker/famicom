namespace Famicom.CPU.RP6502

type Status =
  { mutable n: bool
    mutable v: bool
    mutable d: bool
    mutable i: bool
    mutable z: bool
    mutable c: bool }

module Status =

  let init () =
    { n = false
      v = false
      d = false
      i = true
      z = false
      c = false }


  let pack status =
    let value =
      (if status.n then 0x80 else 0) |||
      (if status.v then 0x40 else 0) |||
      (if status.d then 0x08 else 0) |||
      (if status.i then 0x04 else 0) |||
      (if status.z then 0x02 else 0) |||
      (if status.c then 0x01 else 0) ||| 0x30

    uint8 value


  let unpack value status =
    status.n <- (value &&& 0x80uy) <> 0uy
    status.v <- (value &&& 0x40uy) <> 0uy
    status.d <- (value &&& 0x08uy) <> 0uy
    status.i <- (value &&& 0x04uy) <> 0uy
    status.z <- (value &&& 0x02uy) <> 0uy
    status.c <- (value &&& 0x01uy) <> 0uy
