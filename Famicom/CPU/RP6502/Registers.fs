namespace Famicom.CPU.RP6502

type Registers =
  { mutable a: uint8
    mutable x: uint8
    mutable y: uint8
    mutable s: uint8
    mutable pc: uint16 }

module Registers =

  let init () =
    { a = 0uy
      x = 0uy
      y = 0uy
      s = 0uy
      pc = 0us }


  let sp regs =
    0x0100us ||| (uint16 regs.s)


  let pcl regs =
    uint8 (regs.pc >>> 0)


  let pch regs =
    uint8 (regs.pc >>> 8)
