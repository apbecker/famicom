module Famicom.CPU.RP6502.Codes

let private branch getter = Implied <| fun core ->
  let data = IO.fetch core

  if getter core then
    core |> IO.read core.registers.pc |> IO.dummy

    let pch = Registers.pch core.registers
    let pcl = Registers.pcl core.registers + data

    core.registers.pc <- IO.makeWord pch pcl

    let carry = pcl < data
    let sign = data > 0x7fuy

    if sign <> carry then
      core |> IO.last
      core |> IO.read core.registers.pc |> IO.dummy

      core.registers.pc <-
        if sign then
          core.registers.pc - 0x0100us
        else
          core.registers.pc + 0x0100us


let opAdc = Reader ALU.adc


let opAnd = Reader ALU.and'


let opAsl = Modifier ALU.asl


let opBpl = branch (fun core -> core.status.n |> not)
let opBmi = branch (fun core -> core.status.n)
let opBvc = branch (fun core -> core.status.v |> not)
let opBvs = branch (fun core -> core.status.v)
let opBne = branch (fun core -> core.status.z |> not)
let opBeq = branch (fun core -> core.status.z)
let opBcc = branch (fun core -> core.status.c |> not)
let opBcs = branch (fun core -> core.status.c)


let opBit = Reader <| fun data core ->
  core.status.n <- (data &&& 0x80uy) <> 0uy
  core.status.v <- (data &&& 0x40uy) <> 0uy
  core.status.z <- ALU.zero (data &&& core.registers.a)


let opBrk = Implied <| fun core ->
  core.registers.pc <-
    if core.interrupts.available then
      core.registers.pc - 1us
    else
      core.registers.pc + 1us

  if core.interrupts.res then
    core |> IO.read (Registers.sp core.registers) |> IO.dummy
    core.registers.s <- core.registers.s - 1uy
    core |> IO.read (Registers.sp core.registers) |> IO.dummy
    core.registers.s <- core.registers.s - 1uy
    core |> IO.read (Registers.sp core.registers) |> IO.dummy
    core.registers.s <- core.registers.s - 1uy
  else
    let flag =
      if core.interrupts.available then
        (Status.pack core.status) &&& 0xefuy
      else
        (Status.pack core.status)

    core |> IO.push (Registers.pch core.registers)
    core |> IO.push (Registers.pcl core.registers)
    core |> IO.push flag

  let vector = core.interrupts |> Interrupts.getVector

  core.status.i <- true

  let pcl = core |> IO.read (vector + 0us)
  core |> IO.last
  let pch = core |> IO.read (vector + 1us)

  core.registers.pc <- IO.makeWord pch pcl


let opClc = Implied <| fun core ->
  core.status.c <- false


let opCld = Implied <| fun core ->
  core.status.d <- false


let opCli = Implied <| fun core ->
  core.status.i <- false


let opClv = Implied <| fun core ->
  core.status.v <- false


let opCmp = Reader <| fun data core ->
  core |> ALU.cmp core.registers.a data |> IO.dummy


let opCpx = Reader <| fun data core ->
  core |> ALU.cmp core.registers.x data |> IO.dummy


let opCpy = Reader <| fun data core ->
  core |> ALU.cmp core.registers.y data |> IO.dummy


let opDec = Modifier <| fun data core ->
  core |> ALU.mov (data - 1uy)


let opDex = Implied <| fun core ->
  core.registers.x <- core |> ALU.mov (core.registers.x - 1uy)


let opDey = Implied <| fun core ->
  core.registers.y <- core |> ALU.mov (core.registers.y - 1uy)


let opEor = Reader <| ALU.eor


let opInc = Modifier <| fun data core ->
  ALU.mov (data + 1uy) core


let opInx = Implied <| fun core ->
  core.registers.x <- core |> ALU.mov (core.registers.x + 1uy)


let opIny = Implied <| fun core ->
  core.registers.y <- core |> ALU.mov (core.registers.y + 1uy)


let opJmi = Implied <| fun core ->
  let eal = core |> IO.fetch
  let eah = core |> IO.fetch

  let pcl = core |> IO.read (IO.makeWord eah eal)
  core |> IO.last
  let pch = core |> IO.read (IO.makeWord eah (eal + 1uy))

  core.registers.pc <- IO.makeWord pch pcl


let opJmp = Implied <| fun core ->
  let pcl = core |> IO.fetch
  core |> IO.last
  let pch = core |> IO.fetch

  core.registers.pc <- IO.makeWord pch pcl


let opJsr = Implied <| fun core ->
  let pcl = core |> IO.fetch

  core |> IO.read (core.registers |> Registers.sp) |> IO.dummy
  core |> IO.push (core.registers |> Registers.pch)
  core |> IO.push (core.registers |> Registers.pcl)

  core |> IO.last

  let pch = core |> IO.fetch

  core.registers.pc <- IO.makeWord pch pcl


let opLda = Reader <| fun data core ->
  core.registers.a <- ALU.mov data core


let opLdx = Reader <| fun data core ->
  core.registers.x <- ALU.mov data core


let opLdy = Reader <| fun data core ->
  core.registers.y <- ALU.mov data core


let opLsr = Modifier ALU.lsr'


let opOra = Reader ALU.ora


let opNop = Reader (fun _data _core -> ())


let opPha = Implied <| fun core ->
  core |> IO.last
  core |> IO.push core.registers.a


let opPhp = Implied <| fun core ->
  core |> IO.last
  core |> IO.push (Status.pack core.status)


let opPla = Implied <| fun core ->
  core |> IO.read (Registers.sp core.registers) |> IO.dummy
  core |> IO.last
  core.registers.a <- ALU.mov (IO.pull core) core


let opPlp = Implied <| fun core ->
  core |> IO.read (Registers.sp core.registers) |> IO.dummy
  core |> IO.last
  core.status |> Status.unpack (IO.pull core)


let opRol = Modifier ALU.rol


let opRor = Modifier ALU.ror


let opRti = Implied <| fun core ->
  core |> IO.read (Registers.sp core.registers) |> IO.dummy
  core.status |> Status.unpack (IO.pull core)

  let pcl = core |> IO.pull
  core |> IO.last
  let pch = core |> IO.pull

  core.registers.pc <- IO.makeWord pch pcl


let opRts = Implied <| fun core ->
  core |> IO.read (Registers.sp core.registers) |> IO.dummy

  let pcl = core |> IO.pull
  let pch = core |> IO.pull

  core.registers.pc <- IO.makeWord pch pcl
  core |> IO.last
  core |> IO.fetch |> IO.dummy


let opSbc = Reader ALU.sbc


let opSec = Implied <| fun core ->
  core.status.c <- true


let opSed = Implied <| fun core ->
  core.status.d <- true


let opSei = Implied <| fun core ->
  core.status.i <- true


let opSta = Writer <| fun core ->
  core.registers.a


let opStx = Writer <| fun core ->
  core.registers.x


let opSty = Writer <| fun core ->
  core.registers.y


let opTax = Implied <| fun core ->
  core.registers.x <- core |> ALU.mov core.registers.a


let opTay = Implied <| fun core ->
  core.registers.y <- core |> ALU.mov core.registers.a


let opTsx = Implied <| fun core ->
  core.registers.x <- core |> ALU.mov core.registers.s


let opTxa = Implied <| fun core ->
  core.registers.a <- core |> ALU.mov core.registers.x


let opTxs = Implied <| fun core ->
  core.registers.s <- core.registers.x


let opTya = Implied <| fun core ->
  core.registers.a <- core |> ALU.mov core.registers.y


// Unofficial codes


let opAax = Writer <| fun core ->
  core.registers.a &&& core.registers.x


let opAnc = Reader <| fun data core ->
  core |> ALU.and' data
  core.status.c <- ALU.bit 7 core.registers.a


let opArr = Reader <| fun data core ->
  core |> ALU.and' data
  core.registers.a <- core |> ALU.ror core.registers.a

  core.status.c <- (ALU.bit 6 core.registers.a)
  core.status.v <- (ALU.bit 5 core.registers.a) <> core.status.c


let opAsr = Reader <| fun data core ->
  core |> ALU.and' data
  core.registers.a <- core |> ALU.lsr' core.registers.a


let opAxa = Writer <| fun core ->
  core.registers.a &&& core.registers.x &&& 7uy


let opAxs = Reader <| fun data core ->
  core.registers.x <- core |> ALU.cmp (core.registers.a &&& core.registers.x) data


let opDcp = Modifier <| fun data core ->
  let data = core |> ALU.mov (data - 1uy)
  core |> ALU.cmp core.registers.a data |> IO.dummy
  data


let opIsc = Modifier <| fun data core ->
  let data = core |> ALU.mov (data + 1uy)
  core |> ALU.sbc data
  data


let opJam = Implied <| fun _ ->
  failwith "Keep on jammin'!"


let opLar = Reader <| fun data core ->
  core.registers.s <- core.registers.s &&& data
  core.registers.x <- core |> ALU.mov core.registers.s
  core.registers.a <- core.registers.x


let opLax = Reader <| fun data core ->
  core.registers.x <- core |> ALU.mov data
  core.registers.a <- core.registers.x


let opRla = Modifier <| fun data core ->
  let data = core |> ALU.rol data
  core |> ALU.and' data
  data


let opRra = Modifier <| fun data core ->
  let data = core |> ALU.ror data
  core |> ALU.adc data
  data


let opSlo = Modifier <| fun data core ->
  let data = core |> ALU.asl data
  core |> ALU.ora data
  data


let opSre = Modifier <| fun data core ->
  let data = core |> ALU.lsr' data
  core |> ALU.eor data
  data


let opSxa = Implied <| fun core ->
  let mutable eal = IO.fetch core
  let mutable eah = IO.fetch core

  let data = core.registers.x &&& (eah + 1uy)

  eal <- eal + core.registers.y
  core |> IO.read (IO.makeWord eah eal) |> IO.dummy

  if eal < core.registers.y then
    eah <- data

  core |> IO.write (IO.makeWord eah eal) data


let opSya = Implied <| fun core ->
  let mutable eal = IO.fetch core
  let mutable eah = IO.fetch core

  let data = core.registers.y &&& (eah + 1uy)

  eal <- eal + core.registers.x
  core |> IO.read (IO.makeWord eah eal) |> IO.dummy

  if eal < core.registers.x then
    eah <- data

  core |> IO.write (IO.makeWord eah eal) data


let opXaa = Reader <| fun data core ->
  core.registers.a <- core |> ALU.mov (core.registers.x &&& data)


let opXas = Implied <| fun core ->
  let (Address f) = Address.amAby
  let address = f true core
  let eah = uint8 (address >>> 8)

  core.registers.s <- (core.registers.a &&& core.registers.x)

  core |> IO.last
  core |> IO.write address (core.registers.s &&& (eah + 1uy))
