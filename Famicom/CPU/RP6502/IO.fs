module Famicom.CPU.RP6502.IO

let dummy (_: uint8) = ()


let makeWord (upper: uint8) (lower: uint8) =
  let upper = uint16 upper
  let lower = uint16 lower
  (upper <<< 8) ||| lower


let last core =
  core.interrupts |> Interrupts.poll core.status.i


let read address core =
  core.tick ()

  core.dataBus <- core.read address core.dataBus
  core.dataBus


let write address data core =
  core.tick ()

  core.dataBus <- data
  core.write address core.dataBus


let pull core =
  core.registers.s <- core.registers.s + 1uy
  core |> read (Registers.sp core.registers)


let push value core =
  core |> write (Registers.sp core.registers) value
  core.registers.s <- core.registers.s - 1uy


let fetch core =
  let result = read core.registers.pc core
  core.registers.pc <- core.registers.pc + 1us
  result
