module Famicom.CPU.RP6502.ALU

let bit n data = (data &&& (1uy <<< n)) <> 0uy


let zero data = data = 0x00uy


let mov data core =
  core.status.n <- bit 7 data
  core.status.z <- zero data
  data


let adc data core =
  let flag = if core.status.c then 1uy else 0uy
  let temp = (core.registers.a + data) + flag
  let bits = (core.registers.a ^^^ temp) &&& ~~~(core.registers.a ^^^ data)

  core.status.v <- bit 7 <| bits
  core.status.c <- bit 7 <| bits ^^^ core.registers.a ^^^ data ^^^ temp

  core.registers.a <- mov temp core


let sbc data core =
  adc (~~~data) core


let cmp left data core =
  core.status.c <- left >= data
  core |> mov (left - data)


let and' data core =
  core.registers.a <- core |> mov (core.registers.a &&& data)


let eor data core =
  core.registers.a <- core |> mov (core.registers.a ^^^ data)


let ora data core =
  core.registers.a <- core |> mov (core.registers.a ||| data)


let private shl carry data core =
  core.status.c <- bit 7 data
  core |> mov ((data <<< 1) ||| (if carry then 0x01uy else 0uy))


let private shr carry data core =
  core.status.c <- bit 0 data
  core |> mov ((data >>> 1) ||| (if carry then 0x80uy else 0uy))


let asl data core = shl false data core


let lsr' data core = shr false data core


let rol data core = shl core.status.c data core


let ror data core = shr core.status.c data core
