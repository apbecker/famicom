namespace Famicom.CPU.RP6502

type Interrupts =
  { mutable irq: bool
    mutable nmi: bool
    mutable nmiLatch: bool
    mutable res: bool
    mutable available: bool }

module Interrupts =

  module Vector =

    let [<Literal>] Nmi = 0xfffaus
    let [<Literal>] Res = 0xfffcus
    let [<Literal>] Irq = 0xfffeus


  let init () =
    { irq = false
      nmi = false
      nmiLatch = false
      res = false
      available = false }


  let poll i ints =
    ints.available <- ints.res || ints.nmi || (ints.irq && (not i))


  let getVector ints =
    if ints.res then
      ints.res <- false
      Vector.Res
    else
      if ints.nmi then
        ints.nmi <- false
        Vector.Nmi
      else
        ints.irq <- false
        Vector.Irq
