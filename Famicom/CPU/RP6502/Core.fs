namespace Famicom.CPU.RP6502

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Core =

  let init tick read write =
    { tick = tick
      read = read
      write = write
      status = Status.init ()
      interrupts = Interrupts.init ()
      registers = Registers.init ()
      dataBus = 0uy }


  let reset hard core =
    if hard then
      core.registers.pc <- 0x0000us

      core.registers.a <- 0uy
      core.registers.x <- 0uy
      core.registers.y <- 0uy
      core.registers.s <- 0uy
      core.status |> Status.unpack 0x34uy

    core.interrupts.res <- true
    core.interrupts |> Interrupts.poll core.status.i


  let irq value core =
    core.interrupts.irq <- value


  let nmi value core =
    if core.interrupts.nmiLatch < value then
      core.interrupts.nmi <- true

    core.interrupts.nmiLatch <- value


  let step core =
    let mutable code = IO.fetch core

    if core.interrupts.available then
      code <- 0uy

    core |> Instruction.run code
