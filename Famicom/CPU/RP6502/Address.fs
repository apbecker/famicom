namespace Famicom.CPU.RP6502

type Address =
  | Address of (bool -> Core -> uint16)

module Address =

  let private offset16 write index eah eal core =
    let mutable eah = eah
    let mutable eal = eal + index

    let carry = eal < index
    if carry || write then
      core |> IO.read (IO.makeWord eah eal) |> IO.dummy

      if carry then
        eah <- eah + 1uy

    IO.makeWord eah eal


  let amAbs = Address (fun _write core ->
    let eal = core |> IO.fetch
    let eah = core |> IO.fetch

    IO.makeWord eah eal)


  let private amAbi getter = Address (fun write core ->
    let eal = core |> IO.fetch
    let eah = core |> IO.fetch

    offset16 write (getter core) eah eal core)


  let amAbx = amAbi (fun core -> core.registers.x)


  let amAby = amAbi (fun core -> core.registers.y)


  let amImm = Address (fun _write core ->
    let address = core.registers.pc
    core.registers.pc <- core.registers.pc + 1us
    address)


  let amImp = Address (fun _write core ->
    core.registers.pc)


  let amInx = Address (fun _write core ->
    let ptr = core |> IO.fetch

    core |> IO.read core.registers.pc |> IO.dummy

    let ptr = ptr + core.registers.x
    let eal = core |> IO.read (uint16 (ptr + 0uy))
    let eah = core |> IO.read (uint16 (ptr + 1uy))

    IO.makeWord eah eal)


  let amIny = Address (fun write core ->
    let ptr = core |> IO.fetch
    let eal = core |> IO.read (uint16 (ptr + 0uy))
    let eah = core |> IO.read (uint16 (ptr + 1uy))

    offset16 write core.registers.y eah eal core)


  let amZpg = Address (fun _write core ->
    let ptr = IO.fetch core

    uint16 ptr)


  let private amZpi getter = Address (fun _write core ->
    let eal = core |> IO.fetch

    core |> IO.read (uint16 eal) |> IO.dummy

    uint16 (eal + getter core))


  let amZpx = amZpi (fun core -> core.registers.x)


  let amZpy = amZpi (fun core -> core.registers.y)
