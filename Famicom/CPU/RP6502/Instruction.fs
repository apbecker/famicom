namespace Famicom.CPU.RP6502

type Instruction =
  private
    | Instruction of (Core -> unit)

module Instruction =

  open Famicom.CPU.RP6502.Address
  open Famicom.CPU.RP6502.Codes


  let private a (Modifier code) = Instruction (fun core ->
    core.registers.a <- core |> code core.registers.a)


  let private i (Implied code) = Instruction (fun core ->
    let address = core.registers.pc
    core |> IO.last
    core |> IO.read address |> IO.dummy
    core |> code)


  let private r (Reader code) (Address mode) = Instruction (fun core ->
    let address = mode false core
    core |> IO.last
    let data = core |> IO.read address
    code data core)


  let private m (Modifier code) (Address mode) = Instruction (fun core ->
    let address = mode true core
    let data = core |> IO.read address
    core |> IO.write address data
    core |> IO.last
    core |> IO.write address (code data core))


  let private w (Writer code) (Address mode) = Instruction (fun core ->
    let address = mode true core
    let data = code core
    core |> IO.last
    core |> IO.write address data)


  let private instructions =
    List.toArray <|
      [ i opBrk         // $00
        r opOra amInx   // $01
        i opJam         // $02
        m opSlo amInx   // $03
        r opNop amZpg   // $04
        r opOra amZpg   // $05
        m opAsl amZpg   // $06
        m opSlo amZpg   // $07
        i opPhp         // $08
        r opOra amImm   // $09
        a opAsl         // $0a
        r opAnc amImm   // $0b
        r opNop amAbs   // $0c
        r opOra amAbs   // $0d
        m opAsl amAbs   // $0e
        m opSlo amAbs   // $0f
        i opBpl         // $10
        r opOra amIny   // $11
        i opJam         // $12
        m opSlo amIny   // $13
        r opNop amZpx   // $14
        r opOra amZpx   // $15
        m opAsl amZpx   // $16
        m opSlo amZpx   // $17
        i opClc         // $18
        r opOra amAby   // $19
        r opNop amImp   // $1a
        m opSlo amAby   // $1b
        r opNop amAbx   // $1c
        r opOra amAbx   // $1d
        m opAsl amAbx   // $1e
        m opSlo amAbx   // $1f
        i opJsr         // $20
        r opAnd amInx   // $21
        i opJam         // $22
        m opRla amInx   // $23
        r opBit amZpg   // $24
        r opAnd amZpg   // $25
        m opRol amZpg   // $26
        m opRla amZpg   // $27
        i opPlp         // $28
        r opAnd amImm   // $29
        a opRol         // $2a
        r opAnc amImm   // $2b
        r opBit amAbs   // $2c
        r opAnd amAbs   // $2d
        m opRol amAbs   // $2e
        m opRla amAbs   // $2f
        i opBmi         // $30
        r opAnd amIny   // $31
        i opJam         // $32
        m opRla amIny   // $33
        r opNop amZpx   // $34
        r opAnd amZpx   // $35
        m opRol amZpx   // $36
        m opRla amZpx   // $37
        i opSec         // $38
        r opAnd amAby   // $39
        r opNop amImp   // $3a
        m opRla amAby   // $3b
        r opNop amAbx   // $3c
        r opAnd amAbx   // $3d
        m opRol amAbx   // $3e
        m opRla amAbx   // $3f
        i opRti         // $40
        r opEor amInx   // $41
        i opJam         // $42
        m opSre amInx   // $43
        r opNop amZpg   // $44
        r opEor amZpg   // $45
        m opLsr amZpg   // $46
        m opSre amZpg   // $47
        i opPha         // $48
        r opEor amImm   // $49
        a opLsr         // $4a
        r opAsr amImm   // $4b
        i opJmp         // $4c
        r opEor amAbs   // $4d
        m opLsr amAbs   // $4e
        m opSre amAbs   // $4f
        i opBvc         // $50
        r opEor amIny   // $51
        i opJam         // $52
        m opSre amIny   // $53
        r opNop amZpx   // $54
        r opEor amZpx   // $55
        m opLsr amZpx   // $56
        m opSre amZpx   // $57
        i opCli         // $58
        r opEor amAby   // $59
        r opNop amImp   // $5a
        m opSre amAby   // $5b
        r opNop amAbx   // $5c
        r opEor amAbx   // $5d
        m opLsr amAbx   // $5e
        m opSre amAbx   // $5f
        i opRts         // $60
        r opAdc amInx   // $61
        i opJam         // $62
        m opRra amInx   // $63
        r opNop amZpg   // $64
        r opAdc amZpg   // $65
        m opRor amZpg   // $66
        m opRra amZpg   // $67
        i opPla         // $68
        r opAdc amImm   // $69
        a opRor         // $6a
        r opArr amImm   // $6b
        i opJmi         // $6c
        r opAdc amAbs   // $6d
        m opRor amAbs   // $6e
        m opRra amAbs   // $6f
        i opBvs         // $70
        r opAdc amIny   // $71
        i opJam         // $72
        m opRra amIny   // $73
        r opNop amZpx   // $74
        r opAdc amZpx   // $75
        m opRor amZpx   // $76
        m opRra amZpx   // $77
        i opSei         // $78
        r opAdc amAby   // $79
        r opNop amImp   // $7a
        m opRra amAby   // $7b
        r opNop amAbx   // $7c
        r opAdc amAbx   // $7d
        m opRor amAbx   // $7e
        m opRra amAbx   // $7f
        r opNop amImm   // $80
        w opSta amInx   // $81
        r opNop amImm   // $82
        w opAax amInx   // $83
        w opSty amZpg   // $84
        w opSta amZpg   // $85
        w opStx amZpg   // $86
        w opAax amZpg   // $87
        i opDey         // $88
        r opNop amImm   // $89
        i opTxa         // $8a
        r opXaa amImm   // $8b
        w opSty amAbs   // $8c
        w opSta amAbs   // $8d
        w opStx amAbs   // $8e
        w opAax amAbs   // $8f
        i opBcc         // $90
        w opSta amIny   // $91
        i opJam         // $92
        w opAxa amIny   // $93
        w opSty amZpx   // $94
        w opSta amZpx   // $95
        w opStx amZpy   // $96
        w opAax amZpy   // $97
        i opTya         // $98
        w opSta amAby   // $99
        i opTxs         // $9a
        i opXas         // $9b
        i opSya         // $9c
        w opSta amAbx   // $9d
        i opSxa         // $9e
        w opAxa amAby   // $9f
        r opLdy amImm   // $a0
        r opLda amInx   // $a1
        r opLdx amImm   // $a2
        r opLax amInx   // $a3
        r opLdy amZpg   // $a4
        r opLda amZpg   // $a5
        r opLdx amZpg   // $a6
        r opLax amZpg   // $a7
        i opTay         // $a8
        r opLda amImm   // $a9
        i opTax         // $aa
        r opLax amImm   // $ab
        r opLdy amAbs   // $ac
        r opLda amAbs   // $ad
        r opLdx amAbs   // $ae
        r opLax amAbs   // $af
        i opBcs         // $b0
        r opLda amIny   // $b1
        i opJam         // $b2
        r opLax amIny   // $b3
        r opLdy amZpx   // $b4
        r opLda amZpx   // $b5
        r opLdx amZpy   // $b6
        r opLax amZpy   // $b7
        i opClv         // $b8
        r opLda amAby   // $b9
        i opTsx         // $ba
        r opLar amAby   // $bb
        r opLdy amAbx   // $bc
        r opLda amAbx   // $bd
        r opLdx amAby   // $be
        r opLax amAby   // $bf
        r opCpy amImm   // $c0
        r opCmp amInx   // $c1
        r opNop amImm   // $c2
        m opDcp amInx   // $c3
        r opCpy amZpg   // $c4
        r opCmp amZpg   // $c5
        m opDec amZpg   // $c6
        m opDcp amZpg   // $c7
        i opIny         // $c8
        r opCmp amImm   // $c9
        i opDex         // $ca
        r opAxs amImm   // $cb
        r opCpy amAbs   // $cc
        r opCmp amAbs   // $cd
        m opDec amAbs   // $ce
        m opDcp amAbs   // $cf
        i opBne         // $d0
        r opCmp amIny   // $d1
        i opJam         // $d2
        m opDcp amIny   // $d3
        r opNop amZpx   // $d4
        r opCmp amZpx   // $d5
        m opDec amZpx   // $d6
        m opDcp amZpx   // $d7
        i opCld         // $d8
        r opCmp amAby   // $d9
        r opNop amImp   // $da
        m opDcp amAby   // $db
        r opNop amAbx   // $dc
        r opCmp amAbx   // $dd
        m opDec amAbx   // $de
        m opDcp amAbx   // $df
        r opCpx amImm   // $e0
        r opSbc amInx   // $e1
        r opNop amImm   // $e2
        m opIsc amInx   // $e3
        r opCpx amZpg   // $e4
        r opSbc amZpg   // $e5
        m opInc amZpg   // $e6
        m opIsc amZpg   // $e7
        i opInx         // $e8
        r opSbc amImm   // $e9
        r opNop amImp   // $ea
        r opSbc amImm   // $eb
        r opCpx amAbs   // $ec
        r opSbc amAbs   // $ed
        m opInc amAbs   // $ee
        m opIsc amAbs   // $ef
        i opBeq         // $f0
        r opSbc amIny   // $f1
        i opJam         // $f2
        m opIsc amIny   // $f3
        r opNop amZpx   // $f4
        r opSbc amZpx   // $f5
        m opInc amZpx   // $f6
        m opIsc amZpx   // $f7
        i opSed         // $f8
        r opSbc amAby   // $f9
        r opNop amImp   // $fa
        m opIsc amAby   // $fb
        r opNop amAbx   // $fc
        r opSbc amAbx   // $fd
        m opInc amAbx   // $fe
        m opIsc amAbx ] // $ff


  let run (code: uint8) core =
    let (Instruction instruction) = instructions.[int code]
    core |> instruction
