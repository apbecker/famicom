namespace Famicom.Boards

type CartridgeBoard =
  | NROM of CartridgeImage
  | UxROM of CartridgeImage * Discrete.UxROM

module CartridgeBoard =

  module Default =

    open Famicom.Memory


    let readCPU image address =
      if (address &&& 0x8000) = 0x8000 then
        ValueSome <| Memory.read image.prg address
      else
        ValueNone


    let readPPU image address =
      if (address &&& 0x2000) = 0x0000 then
        ValueSome <| Memory.read image.chr address
      else
        ValueNone


    let writePPU image address data =
      if (address &&& 0x2000) = 0x0000 then
        address |> Memory.write image.chr data


    let ciram image address =
      let x = (address >>> 10) &&& image.h
      let y = (address >>> 11) &&& image.v

      ValueSome (x ||| y)


  let readCPU cartridgeBoard address =
    match cartridgeBoard with
    | NROM(image) ->
      Default.readCPU image address

    | UxROM(image, state) ->
      Discrete.UxROM.readCPU image state address


  let writeCPU cartridgeBoard address (data: uint8) =
    match cartridgeBoard with
    | NROM(_) ->
      ()

    | UxROM(_, state) ->
      Discrete.UxROM.writeCPU state address (int data)


  let readPPU cartridgeBoard address =
    match cartridgeBoard with
    | NROM(image) ->
      Default.readPPU image address

    | UxROM(image, _) ->
      Default.readPPU image address


  let writePPU cartridgeBoard address data =
    match cartridgeBoard with
    | NROM(image) ->
      Default.writePPU image address data

    | UxROM(image, _) ->
      Default.writePPU image address data


  let ciram cartridgeBoard address =
    match cartridgeBoard with
    | NROM(image) ->
      Default.ciram image address

    | UxROM(image, _) ->
      Default.ciram image address


  let (|Pattern|_|) pattern s =
    if System.Text.RegularExpressions.Regex.IsMatch(s, pattern) then
      Some()
    else
      None


  let tryInit binary =
    let image = CartridgeImage.create(binary)

    match image.mapper with
    | Pattern("(HVC|NES)-NROM-(128|256)") ->
      Ok <| NROM(image)

    | Pattern("(HVC|NES)-U[NO]ROM") ->
      Ok <| UxROM(image, Discrete.UxROM.init())

    | mapper ->
      Error <| sprintf "Unsupported cartridge board: %s" mapper
