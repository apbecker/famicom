namespace Famicom.Boards.Discrete

type UxROM =
  { mutable prgPage: int }

module UxROM =

  open Famicom.Boards
  open Famicom.Memory


  let init () =
    { prgPage = 0 }


  let private mapCPU state address =
    if (address &&& 0xc000) = 0x8000 then
      (address &&& 0x3fff) ||| (state.prgPage <<< 14)
    else
      (address &&& 0x3fff) ||| (~~~0 <<< 14)


  let readCPU image state address =
    if (address &&& 0x8000) = 0x8000 then
      ValueSome <| Memory.read image.prg (mapCPU state address)
    else
      ValueNone


  let writeCPU state address data =
    if (address &&& 0x8000) = 0x8000 then
      state.prgPage <- int data
