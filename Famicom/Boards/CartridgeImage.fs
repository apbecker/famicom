namespace Famicom.Boards

open System
open System.IO
open System.Linq
open Famicom
open Famicom.Memory

type CartridgeImage =
  { prg: Memory
    chr: Memory
    wram: Memory
    vram: Memory
    h: int
    v: int
    mapper: string }

module CartridgeImage =

  let private tryParse (str: string) =
    match Int32.TryParse(str) with
    | false, _ ->
      None

    | true, value ->
      Some(value)


  let private left count (str: string) =
    if str.Length < count then
      None
    else
      Some <| str.Substring(0, count)


  let private valueOfSizeString (str: string) =
    Option.ofObj str
      |> Option.bind (fun str -> left (str.Length - 1) str)
      |> Option.bind tryParse
      |> Option.map (fun kib -> kib * 1024)
      |> Option.defaultValue 0


  let private wramToMemory (wram: Database.Wram) =
    valueOfSizeString wram.Size
      |> Memory.ram


  let private vramToMemory (vram: Database.Vram) =
    valueOfSizeString vram.Size
      |> Memory.ram


  let private prgToMemory getBytes (prg: Database.Prg) =
    valueOfSizeString prg.Size
      |> getBytes
      |> Memory.rom


  let private chrToMemory getBytes (chr: Database.Chr) =
    valueOfSizeString chr.Size
      |> getBytes
      |> Memory.rom


  let private boolToInt =
    function
    | false -> 0
    | true -> 1


  let private skipHeader (stream: Stream) =
    stream.Seek(16L, SeekOrigin.Begin) |> ignore


  let private boardToImage (binary: byte array) (board: Database.Board) =
    use stream = new MemoryStream(binary)
    use reader = new BinaryReader(stream)

    skipHeader stream

    let prgRoms = board.Prgs |> Seq.map (prgToMemory reader.ReadBytes)
    let chrRoms = board.Chrs |> Seq.map (chrToMemory reader.ReadBytes)
    let chrRams = board.Vrams |> Seq.map vramToMemory

    let wram = board.Wrams |> Seq.map wramToMemory
    let vram = board.Vrams |> Seq.map vramToMemory

    { prg = prgRoms.First()
      chr = chrRoms.Concat(chrRams).First()
      wram = wram.FirstOrDefault()
      vram = vram.FirstOrDefault()
      mapper = board.Type
      h = board.Pad |> Option.map (fun pad -> boolToInt pad.H) |> Option.defaultValue 0
      v = board.Pad |> Option.map (fun pad -> boolToInt pad.V) |> Option.defaultValue 0 }


  let private defaultBoard =
    Database.Board
      ( "NES-SxROM",
        "NES-SxROM-01",
        "1",
        [| Database.Prg(None, None, "256k", None, "", "", [||]) |], // PRG
        [||], // [| Database.Chr(None, None,  "8k", None, "", "", [||]) |], // CHR
        [||],
        [| Database.Vram(None, None, None, "8k", None, "", "", [||]) |], // VRAM
        [||],
        None,
        None )


  let create binary =
    Database.tryFind binary
      |> Option.defaultValue defaultBoard
      |> boardToImage binary
