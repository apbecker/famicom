namespace Famicom.Memory

type Memory =
  | Ram of byte [] * int
  | Rom of byte [] * int

module Memory =

  let ram capacity =
    let buffer = Array.zeroCreate<byte> capacity
    Ram (buffer, capacity - 1)


  let rom buffer =
    let capacity = Array.length buffer
    Rom (buffer, capacity - 1)


  let read memory address =
    match memory with
    | Ram (buffer, mask) ->
      buffer.[address &&& mask]

    | Rom (buffer, mask) ->
      buffer.[address &&& mask]


  let write memory data address =
    match memory with
    | Ram (buffer, mask) ->
      buffer.[address &&& mask] <- data

    | Rom _ ->
      ()
