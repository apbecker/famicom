module Famicom.Memory.CGRAM

let init () =
  Memory.ram 32


let private mapAddress address =
  match (address &&& 3) with
  | 0 -> address &&& 0x000c
  | _ -> address &&& 0x001f


let read cgram address =
  mapAddress address
    |> Memory.read cgram


let write cgram data address =
  mapAddress address
    |> Memory.write cgram data
